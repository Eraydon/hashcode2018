#include<iostream>
#include<fstream>
#include <vector>
#include<cmath>
#include "Algorithm.cpp"

using namespace std;

//Implementation class
class Ride : public IndexedObject {
protected:
	Intersection departure_m;
	Intersection arrival_m;
	int earliestBegin_m;
	int latestFinish_m;
	int length_m;
public:
	Ride(int);
	void init();
	int getLength() const;
	static int getDistance(const Ride*, const Ride*);
	int getEarliestBegin() const;
	int getLatestFinish() const;
	Intersection getDeparture() const;
	Intersection getArrival() const;
	friend istream& operator>> (istream& is_p, Ride& data_p);
	friend ostream& operator<< (ostream& is_p, const Ride& data_p);
};

Ride::Ride(int index_p):IndexedObject(index_p), length_m(-1)
{
}

void Ride::init(){
	length_m = Intersection::getDistance(departure_m,arrival_m);
}

int Ride::getEarliestBegin() const{
	return earliestBegin_m;
}

int Ride::getLatestFinish() const{
	return latestFinish_m;
}

Intersection Ride::getDeparture() const{
	return departure_m;
}

Intersection Ride::getArrival() const{
	return arrival_m;
}

int Ride::getDistance(const Ride* firstRide_p, const Ride* secondRide_p){
	if (!secondRide_p) return 0;
	if (!firstRide_p) return Intersection::getDistance(Intersection(0,0),secondRide_p->departure_m);
	return Intersection::getDistance(firstRide_p->arrival_m, secondRide_p->departure_m);
}

//Implementation class
class Vehicle : public IndexedObject {
protected:
	friend ostream& operator<< (ostream& is_p, const Vehicle& data_p);
public:
	Vehicle(int);
};

Vehicle::Vehicle(int index_p): IndexedObject(index_p)
{
}

int Ride::getLength() const{
	return length_m;
}

ostream& operator<<(ostream& os_p, const Vehicle& data_p){
	//Reading the header
	os_p << data_p.index_m;
	return os_p;
}

istream& operator>>(istream& is_p, Ride& data_p){
	//Reading the header
	is_p >> data_p.departure_m.x >> data_p.departure_m.y >> data_p.arrival_m.x >> data_p.arrival_m.y >> data_p.earliestBegin_m >> data_p.latestFinish_m;
	return is_p;
}

ostream& operator<<(ostream& os_p, const Ride& data_p){
	//Reading the header
//	os_p << data_p.departure_m.x << " " << data_p.departure_m.y << " " << data_p.arrival_m.x << " " << data_p.arrival_m.y << " " << data_p.earliestBegin_m << " " << data_p.latestFinish_m << "\n";
	os_p << data_p.index_m;
	return os_p;
}

class VehicleAssignmentProblem : public AssignmentProblem<Vehicle,Ride>{
protected :
	int nbSteps_m;
	int bonus_m;
public:
	int getBonus() const;
	int getNbSteps() const;
	VehicleAssignmentProblem(int&,int&,int&,int&,int&,int&,vector<Ride>&, vector<Vehicle>&);
};

int VehicleAssignmentProblem::getBonus() const{
	return bonus_m;
}

int VehicleAssignmentProblem::getNbSteps() const{
	return nbSteps_m;
}

VehicleAssignmentProblem::VehicleAssignmentProblem(int& vSize_p,int& hSize_p,int& nbVehicles_p,int& nbRides_p,int& bonus_p,int& nbSteps_p, vector<Ride>& rides_p,vector<Vehicle>& vehicles_p):
	AssignmentProblem<Vehicle,Ride>(nbVehicles_p, nbRides_p, rides_p, vehicles_p),
	nbSteps_m(nbSteps_p),
	bonus_m(bonus_p)
{
};

class VehicleWorkAssignmentProblem : public WorkAssignmentProblem<Vehicle,Ride>{
protected :
	//Earliest and latest possible departures wrt the current assignment
public:
	vector<vector<pair<int,int> > > timeSteps_m;
	vector<int> minDistToNext_m;
	VehicleWorkAssignmentProblem(VehicleAssignmentProblem*);
	bool isPossibleAssignment(const Ride*, const Vehicle*,int);
	void assignTaskToAssignee(const Ride*,const Vehicle*,int);
	int singleEval(const Ride*, const Vehicle*) const;
	void deleteTask(const Vehicle*, int);
	int updateEarliestTimer(const Vehicle*,int);
	void updateEarliestTimers(const Vehicle*,int);
	int updateLatestTimer(const Vehicle*,int);
	void updateLatestTimers(const Vehicle*,int);
};

VehicleWorkAssignmentProblem::VehicleWorkAssignmentProblem(VehicleAssignmentProblem* pb_p):
	WorkAssignmentProblem<Vehicle,Ride>(pb_p)
{
	for (int  i = 0; i < problem_m->getNbTask(); i++){
		timeSteps_m.insert(timeSteps_m.begin()+i,vector<pair<int,int> >());
	}

	for (int  i = 0; i < problem_m->getNbTask(); i++){
		int minValue_l = static_cast<const VehicleAssignmentProblem*>(problem_m)->getNbSteps();
		for (int j = 0; j < problem_m->getNbTask();j++){
			if (isAlreadyAssigned(pb_p->getTask(j))){
				continue;
			}
			minValue_l = min(minValue_l,Ride::getDistance(pb_p->getTask(i),pb_p->getTask(j)));
		}
		minDistToNext_m.insert(minDistToNext_m.begin()+i,minValue_l);
	}
}

bool VehicleWorkAssignmentProblem::isPossibleAssignment(const Ride* ride_p, const Vehicle* vehicle_p, int index_p){
	if (!vehicle_p){
		return true;
	}

	bool isPossible_l = true;

	//Accelerator
	int vehicleIndex_l = vehicle_p->getIndex();
	vector<pair<int,int> >& tasksSteps_l = timeSteps_m[vehicleIndex_l];

	//Accelerator
	const Ride* nextTask_l = (index_p < (int)(tasksSteps_l.size()-1) ? assignment_m[vehicleIndex_l][index_p] : 0);
	if (nextTask_l && ride_p->getEarliestBegin() + ride_p->getLength() + Ride::getDistance(ride_p,nextTask_l) > tasksSteps_l[index_p].second){
		return false;
	}

	const Ride* lastTask_l = (index_p > 0 ? assignment_m[vehicleIndex_l][index_p-1] : 0);
	if (lastTask_l && tasksSteps_l[index_p-1].first + lastTask_l->getLength() + Ride::getDistance(lastTask_l,ride_p) > ride_p->getLatestFinish() - ride_p->getLength()){
		return false;
	}

	assignTaskToAssignee(ride_p,vehicle_p,index_p);

	for (int i = 0; i < assignment_m[vehicleIndex_l].size(); i++){
		if (timeSteps_m[vehicleIndex_l][i].first + assignment_m[vehicleIndex_l][i]->getLength() > timeSteps_m[vehicleIndex_l][i].second){
			isPossible_l = false;
			break;
		}
	}

	deleteTask(vehicle_p,index_p);
	return isPossible_l;
}

void VehicleWorkAssignmentProblem::assignTaskToAssignee(const Ride* ride_p, const Vehicle* vehicle_p, int index_p){
	WorkAssignmentProblem<Vehicle,Ride>::assignTaskToAssignee(ride_p,vehicle_p,index_p);
	timeSteps_m[vehicle_p->getIndex()].insert(timeSteps_m[vehicle_p->getIndex()].begin()+index_p,pair<int,int>());
	updateEarliestTimers(vehicle_p,index_p);
	updateLatestTimers(vehicle_p,index_p);
	this->value_m += singleEval(ride_p,vehicle_p);
}

void VehicleWorkAssignmentProblem::updateEarliestTimers(const Vehicle* vehicle_p, int index_p){
	int newValue_l = 0;
	int vehicleIndex_l = vehicle_p->getIndex();
	for (int i = index_p; i < assignment_m[vehicleIndex_l].size(); i++){
		int oldValue_l = timeSteps_m[vehicleIndex_l][i].first;
		newValue_l = updateEarliestTimer(vehicle_p,i);
		if (i != index_p && newValue_l == oldValue_l) break;
	}
}

void VehicleWorkAssignmentProblem::updateLatestTimers(const Vehicle* vehicle_p, int index_p){
	int newValue_l = 0;
	int vehicleIndex_l = vehicle_p->getIndex();
	for (int i = index_p; i > -1; i--){
		int oldValue_l = timeSteps_m[vehicleIndex_l][i].second;
		newValue_l = updateLatestTimer(vehicle_p,i);
		if (i != index_p && newValue_l == oldValue_l) break;
	}
}

int VehicleWorkAssignmentProblem::updateEarliestTimer(const Vehicle* vehicle_p, int index_p){
	vector<pair<int,int> >& tasksSteps_l = timeSteps_m[vehicle_p->getIndex()];
	const Ride* currentTask_l = assignment_m[vehicle_p->getIndex()][index_p];
	const Ride* lastTask_l = (index_p > 0 ? assignment_m[vehicle_p->getIndex()][index_p-1] : 0);
	int lastTaskBegin_l = (index_p > 0 ? tasksSteps_l[index_p-1].first : 0);
	int lastTaskLength_l = (lastTask_l ? lastTask_l->getLength() : 0);
	int distance_l = Ride::getDistance(lastTask_l,currentTask_l);
	int newTimeStepBegin_l = max(currentTask_l->getEarliestBegin(), distance_l + lastTaskBegin_l + lastTaskLength_l);
	timeSteps_m[vehicle_p->getIndex()][index_p].first = newTimeStepBegin_l;
	return newTimeStepBegin_l;
}

int VehicleWorkAssignmentProblem::updateLatestTimer(const Vehicle* vehicle_p, int index_p){
	int vehicleIndex_l = vehicle_p->getIndex();
	vector<pair<int,int> >& tasksSteps_l = timeSteps_m[vehicleIndex_l];
	const Ride* currentTask_l = assignment_m[vehicleIndex_l][index_p];
	const Ride* nextTask_l = (index_p < (int)(tasksSteps_l.size()-1) ? assignment_m[vehicleIndex_l][index_p+1] : 0);
	int nextTaskLatestEnd_l = (index_p < (int)(tasksSteps_l.size()-1) ? tasksSteps_l[index_p+1].second : static_cast<const VehicleAssignmentProblem*>(getProblem())->getNbSteps());
	int nextTaskLength_l = (nextTask_l ? nextTask_l->getLength() : 0);
	int distance_l = Ride::getDistance(currentTask_l,nextTask_l);
	int newTimeStepEnd_l = min(currentTask_l->getLatestFinish(), nextTaskLatestEnd_l - nextTaskLength_l - distance_l);
	tasksSteps_l[index_p].second = newTimeStepEnd_l;
	return newTimeStepEnd_l;
}

void VehicleWorkAssignmentProblem::deleteTask(const Vehicle* vehicle_p, int index_p){
	WorkAssignmentProblem<Vehicle,Ride>::deleteTask(vehicle_p,index_p);
	timeSteps_m[vehicle_p->getIndex()].erase(timeSteps_m[vehicle_p->getIndex()].begin()+index_p);
	updateEarliestTimers(vehicle_p,index_p);
	updateLatestTimers(vehicle_p,index_p);
}

int VehicleWorkAssignmentProblem::singleEval(const Ride* ride_p, const Vehicle* vehicle_p) const{
	int score_l = ride_p->getLength();
	if (vehicle_p){
		int vehicleIndex_l = vehicle_p->getIndex();
		int index_l = taskAssignment_m[ride_p->getIndex()].second;
		if (timeSteps_m[vehicleIndex_l][index_l].first == ride_p->getEarliestBegin()){
			score_l += static_cast<const VehicleAssignmentProblem*>(getProblem())->getBonus();
		}
	}
	return score_l;
}

class VehicleSwap: public AssignmentSwap<Vehicle,Ride>{
public:
	VehicleSwap(VehicleWorkAssignmentProblem*);
};

VehicleSwap::VehicleSwap(VehicleWorkAssignmentProblem* wpb_l):
		AssignmentSwap<Vehicle,Ride>(wpb_l)
{
}
