#include<iostream>
#include<fstream>
#include <vector>
#include<map>
#include<cmath>
#include "BasicObjects.cpp"

using namespace std;

/******************************************************************************************************************************************************************************************************************
 *
 * 																					ASSIGNMENT PROBLEM (BO)
 *
 ******************************************************************************************************************************************************************************************************************/

template<typename Assignee, typename Task> class AssignmentProblem{
protected:
	int nbAssignees_m;
	int nbTasks_m;
	vector<Assignee> assignees_m;
	vector<Task> tasks_m;
public:
	AssignmentProblem(int&, int&, vector<Task>&, vector<Assignee>&);
	const Assignee* getAssignee(int) const;
	const Task* getTask(int) const;
	int getNbAssignee() const;
	int getNbTask() const;
};

template<typename Assignee, typename Task> const Assignee* AssignmentProblem<Assignee,Task>::getAssignee(int index_p) const{
	return &assignees_m[index_p];
}

template<typename Assignee, typename Task> const Task* AssignmentProblem<Assignee,Task>::getTask(int index_p) const{
	return &tasks_m[index_p];
}

template<typename Assignee, typename Task> int AssignmentProblem<Assignee,Task>::getNbAssignee() const{
	return assignees_m.size();
}

template<typename Assignee, typename Task> int AssignmentProblem<Assignee,Task>::getNbTask() const{
	return tasks_m.size();
}

/******************************************************************************************************************************************************************************************************************
 *
 * 																					ASSIGNMENT PROBLEM (WORK)
 *
 ******************************************************************************************************************************************************************************************************************/

template<typename Assignee, typename Task> class WorkAssignmentProblem{
protected:
	AssignmentProblem<Assignee,Task>* problem_m;
public:
	vector<vector <const Task*> > assignment_m;
	vector<pair<const Assignee*,int> > taskAssignment_m;
	int value_m;
	WorkAssignmentProblem(AssignmentProblem<Assignee,Task>*);
	const AssignmentProblem<Assignee,Task>* getProblem() const;
	virtual void deleteTask(const Assignee*, int);
	virtual int eval() const;
	void printSolution() const;
	bool isAlreadyAssigned(const Task*);
	virtual void assignTaskToAssignee(const Task*,const Assignee*, int);
	virtual bool isPossibleAssignment(const Task*, const Assignee*,int) = 0;
	virtual int singleEval(const Task*, const Assignee*) const = 0;
};

template<typename Assignee, typename Task> WorkAssignmentProblem<Assignee,Task>::WorkAssignmentProblem(AssignmentProblem<Assignee,Task>* problem_p):problem_m(problem_p),value_m(0){
	for (int i = 0; i < problem_m->getNbAssignee() ; i++){
		assignment_m.insert(assignment_m.begin() + i, vector<const Task*>());
	}

	for (int  i = 0; i < problem_m->getNbTask(); i++){
		taskAssignment_m.insert(taskAssignment_m.begin()+i,pair<const Assignee*,int>(0,0));
	}
}

template<typename Assignee, typename Task> const AssignmentProblem<Assignee,Task>* WorkAssignmentProblem<Assignee,Task>::getProblem() const{
	return problem_m;
}

template<typename Assignee, typename Task> void WorkAssignmentProblem<Assignee,Task>::deleteTask(const Assignee* ass_p, int index_p){
	int assIndex_l = ass_p->getIndex();
	const Task* task_l = assignment_m[assIndex_l][index_p];
	value_m -= singleEval(task_l,ass_p);
	taskAssignment_m[task_l->getIndex()] = pair<const Assignee*,int>(0,0);
	assignment_m[assIndex_l].erase(assignment_m[assIndex_l].begin()+index_p);
}

template<typename Assignee, typename Task> int WorkAssignmentProblem<Assignee,Task>::eval() const{
	int score_l = 0;
	for (int i = 0; i < getProblem()->getNbAssignee(); i++){
		int assigneeScore_l = 0;
		const Assignee* ass_l = getProblem()->getAssignee(i);
		for (unsigned int j = 0; j < assignment_m[i].size(); j++){
			const Task* task_l = assignment_m[i][j];
			assigneeScore_l += singleEval(task_l, ass_l);
		}
		score_l += assigneeScore_l;
		cout << "SCORE OF ASIGNEE " << i << " is " << assigneeScore_l << "\n";
	}
	return score_l;
}

template<typename Assignee,typename Task> void WorkAssignmentProblem<Assignee,Task>::printSolution() const{
	for (int i = 0; i < problem_m->getNbAssignee(); i++){
		vector <const Task*> tasks_l = assignment_m[i];
		cout << tasks_l.size();
		for (unsigned int j = 0; j < tasks_l.size(); j++){
			cout << " " << *tasks_l[j];
		}
		cout << "\n";
	}
}

template<typename Assignee, typename Task> bool WorkAssignmentProblem<Assignee,Task>::isAlreadyAssigned(const Task* task_p){
	return (taskAssignment_m[task_p->getIndex()].first);
}

template<typename Assignee, typename Task> void WorkAssignmentProblem<Assignee,Task>::assignTaskToAssignee(const Task* task_p, const Assignee* ass_p, int index_p){
	//Update assignment
	assignment_m[ass_p->getIndex()].insert(assignment_m[ass_p->getIndex()].begin()+ index_p,task_p);
	//Update taskAssignment_m;
	taskAssignment_m[task_p->getIndex()] = pair<const Assignee*,int>(ass_p,index_p);
}

/******************************************************************************************************************************************************************************************************************
 *
 * 																					ASSIGNMENT ALGORITHMS
 *
 ******************************************************************************************************************************************************************************************************************/

template<typename Assignee, typename Task> class AssignmentProblemAlg{
protected:
	WorkAssignmentProblem<Assignee, Task>* workProblem_m;
public:
	//Algorithm
	AssignmentProblemAlg(WorkAssignmentProblem<Assignee,Task>*);
	virtual void optimize() = 0;
	virtual void init() = 0;
	virtual void update(const Task*,const Assignee*) = 0;
	WorkAssignmentProblem<Assignee,Task>* getWorkProblem() const;
};

template<typename Assignee, typename Task> AssignmentProblemAlg<Assignee,Task>::AssignmentProblemAlg(WorkAssignmentProblem<Assignee,Task>* problem_p):workProblem_m(problem_p)
{
}

template<typename Assignee, typename Task> WorkAssignmentProblem<Assignee,Task>* AssignmentProblemAlg<Assignee,Task>::getWorkProblem() const{
	return workProblem_m;
}

template<typename Assignee, typename Task> AssignmentProblem<Assignee,Task>::AssignmentProblem(int& nbTasks_p, int& nbAssignees_p, vector<Task>& tasks_p, vector<Assignee>& ass_p):
	nbAssignees_m(nbAssignees_p),
	nbTasks_m(nbTasks_p),
	assignees_m(ass_p),
	tasks_m(tasks_p)
{
}

/******************************************************************************************************************************************************************************************************************
 *
 * 																					      GREEDY
 *
 ******************************************************************************************************************************************************************************************************************/

template<typename Assignee, typename Task> class GreedyAssignment: public AssignmentProblemAlg<Assignee,Task>{
protected:
	int currentAssigneeIndex_m;
	int currentTaskIndex_m;
	bool continueToAssignToAssignee_m;
public:
	GreedyAssignment(WorkAssignmentProblem<Assignee,Task>*);
	virtual void optimize();
	virtual void optimizeRange(int,int);
	virtual const Assignee* chooseAssignee();
	bool continueToAssignToAssignee(const Assignee*);
	virtual const Task* chooseTask(const Assignee*);
	virtual bool isBetter(const Task*, const Task*, const Assignee*) = 0;
	virtual void init() = 0;
};

template<typename Assignee, typename Task> void GreedyAssignment<Assignee,Task>::optimizeRange(int indexBegin_p, int indexEnd_p){
	init();
	for (int i = indexBegin_p; i < indexEnd_p; i++){
		const Assignee* ass_l = chooseAssignee();
		while(continueToAssignToAssignee(ass_l)){
			const Task* task_l = chooseTask(ass_l);
			if (!task_l)	break;
			this->update(task_l,ass_l);
			this->getWorkProblem()->assignTaskToAssignee(task_l,ass_l,this->getWorkProblem()->assignment_m[ass_l->getIndex()].size());
		}
	}
}

template<typename Assignee, typename Task> void GreedyAssignment<Assignee,Task>::optimize(){
	optimizeRange(0,this->getWorkProblem()->getProblem()->getNbAssignee());
}

template<typename Assignee, typename Task> GreedyAssignment<Assignee,Task>::GreedyAssignment(WorkAssignmentProblem<Assignee,Task>* pb_p):
		AssignmentProblemAlg<Assignee,Task>(pb_p),
		currentAssigneeIndex_m(-1),
		currentTaskIndex_m(-1),
		continueToAssignToAssignee_m(true)
{
}

template<typename Assignee, typename Task> const Task* GreedyAssignment<Assignee,Task>::chooseTask(const Assignee* ass_p){
	const Task* task_l = 0, *bestTask_l = 0;
	while (currentTaskIndex_m != this->getWorkProblem()->getProblem()->getNbTask()-1){
		task_l = this->getWorkProblem()->getProblem()->getTask(++currentTaskIndex_m);
		if (!this->getWorkProblem()->isAlreadyAssigned(task_l) &&
				isBetter(task_l,bestTask_l,ass_p) &&
				this->getWorkProblem()->isPossibleAssignment(task_l,ass_p,this->getWorkProblem()->assignment_m[ass_p->getIndex()].size())){
			bestTask_l = task_l;
		}
	}
	currentTaskIndex_m = -1;
	return bestTask_l;
}

template<typename Assignee, typename Task> const Assignee* GreedyAssignment<Assignee,Task>::chooseAssignee(){

	const Assignee* ass_l = 0;

	if (currentAssigneeIndex_m != this->getWorkProblem()->getProblem()->getNbAssignee()-1){
		ass_l = this->getWorkProblem()->getProblem()->getAssignee(++currentAssigneeIndex_m);
		this->continueToAssignToAssignee_m = true;
		currentTaskIndex_m = -1;
	}

	return ass_l;
}

template<typename Assignee, typename Task> bool GreedyAssignment<Assignee,Task>::continueToAssignToAssignee(const Assignee* ass_p){
	return this->continueToAssignToAssignee_m;
}

/******************************************************************************************************************************************************************************************************************
 *
 * 																					SWAP
 *
 ******************************************************************************************************************************************************************************************************************/

template<typename Assignee, typename Task> class AssignmentSwap: public AssignmentProblemAlg<Assignee,Task>{
public:
	AssignmentSwap(WorkAssignmentProblem<Assignee,Task>*);
	void optimize();
	void optimizeRange(int,int);
	bool isPossibleRemoval(const Task*, const Assignee*);
	bool wantToSwap(const Task*, const Task*, const Assignee*, const Assignee*);
	void swap(const Task*, const Task*, const Assignee*, const Assignee*,int);
	virtual void init(){};
	virtual void update(const Task*,const Assignee*){};
};

template<typename Assignee, typename Task> void AssignmentSwap<Assignee,Task>::optimize(){
	optimizeRange(0,this->getWorkProblem()->getProblem()->getNbAssignee());
}

template<typename Assignee, typename Task> void AssignmentSwap<Assignee,Task>::optimizeRange(int indexBegin_p, int indexEnd_p){
	cout << "EVAL BEFORE SWAP = " << this->getWorkProblem()->value_m << "\n";
	int evalBefore_l = this->getWorkProblem()->value_m;
	const Task* bestTask_l = 0;

	for (int i = indexBegin_p; i < indexEnd_p; i++){
		vector<const Task*>& assignments_l = this->getWorkProblem()->assignment_m[i];
		const Assignee* firstTaskAssignee_l = this->getWorkProblem()->getProblem()->getAssignee(i);
		for (int j = (int)assignments_l.size()-1; j > 0; j--){
			const Task* firstTaskToSwap_l = this->getWorkProblem()->assignment_m[i][j];
			this->getWorkProblem()->deleteTask(firstTaskAssignee_l,j);
			bestTask_l = firstTaskToSwap_l;
			for (int k = 0; k < this->getWorkProblem()->getProblem()->getNbTask(); k++){
				const Task* secondTaskToSwap_l = this->getWorkProblem()->getProblem()->getTask(k);
				const Assignee* secondTaskAssignee_l = this->getWorkProblem()->taskAssignment_m[k].first;
				if (secondTaskToSwap_l != bestTask_l && wantToSwap(firstTaskToSwap_l,secondTaskToSwap_l,firstTaskAssignee_l, secondTaskAssignee_l)){
					if (!this->getWorkProblem()->isAlreadyAssigned(secondTaskToSwap_l) && this->getWorkProblem()->isPossibleAssignment(secondTaskToSwap_l,firstTaskAssignee_l,j)){
						swap(firstTaskToSwap_l,secondTaskToSwap_l,firstTaskAssignee_l,secondTaskAssignee_l,j);
						int newEval_l = this->getWorkProblem()->value_m;
						if (newEval_l > evalBefore_l){
							evalBefore_l = newEval_l;
							bestTask_l = secondTaskToSwap_l;
						}
						this->getWorkProblem()->deleteTask(firstTaskAssignee_l,j);
					}
				}
			}
			this->getWorkProblem()->assignTaskToAssignee(bestTask_l,firstTaskAssignee_l,j);
		}
	}
}

template<typename Assignee, typename Task> AssignmentSwap<Assignee,Task>::AssignmentSwap(WorkAssignmentProblem<Assignee,Task>* pb_p):
		AssignmentProblemAlg<Assignee,Task>(pb_p)
{
}

template<typename Assignee, typename Task> bool AssignmentSwap<Assignee,Task>::isPossibleRemoval(const Task*, const Assignee*){
	return true;
}

template<typename Assignee, typename Task> void AssignmentSwap<Assignee,Task>::swap(const Task* firstTask_p, const Task* secondTask_p, const Assignee* firstAssignee_p, const Assignee* secondAssignee_p, int j){
	this->getWorkProblem()->assignTaskToAssignee(secondTask_p,firstAssignee_p,j);
}

template<typename Assignee, typename Task> bool AssignmentSwap<Assignee,Task>::wantToSwap(const Task* firstTask_p, const Task* secondTask_p, const Assignee* firstAssignee_p, const Assignee* secondAssignee_p){
	if (!firstAssignee_p || secondAssignee_p) return false;
	return (this->getWorkProblem()->singleEval(firstTask_p,secondAssignee_p) < this->getWorkProblem()->singleEval(secondTask_p,firstAssignee_p));
}
