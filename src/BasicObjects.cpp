#include<iostream>
#include<fstream>
#include <vector>
#include<cmath>

using namespace std;

/************************************************************** Class of intersection in a matrix ***************************************************/

class Intersection{
public:
	int x;
	int y;
	static int getDistance(const Intersection&, const Intersection&);
	Intersection(int,int);
	Intersection();
};

Intersection::Intersection(int x, int y): x(x),y(y){

}

Intersection::Intersection(){

}

inline int Intersection::getDistance(const Intersection& inter1_l, const Intersection& inter2_l){
	return std::abs(inter2_l.x - inter1_l.x) + std::abs(inter2_l.y - inter1_l.y);
}

/******************************************************************** Class of matrix ***************************************************************/

class Matrix{
protected :
	int nbLines_m;
	int nbColumns_m;
public:
	Matrix(int,int);
};

/********************************************************** Class of indexed values, to be inherited ***********************************************/

class IndexedObject{
protected:
	int index_m;
public :
	IndexedObject(int);
	void setIndex(int);
	int getIndex() const;
};

IndexedObject::IndexedObject(int index_p): index_m(index_p){
}

void IndexedObject::setIndex(int index_p){
	index_m = index_p;
}

inline int IndexedObject::getIndex() const{
	return index_m;
}
