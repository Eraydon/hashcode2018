#include<iostream>
#include<fstream>
#include<cstdlib>
#include<vector>

using namespace std;

class Cell{
public:
	Cell(int,int);
	int x_m;
	int y_m;
};

Cell::Cell(int x,int y):
		x_m(x),
		y_m(y)
{
}

class Slice{
public:
	Slice(Cell*,Cell*);
	Cell* topLeftCell_m;
	Cell* bottomRightCell_m;
	friend ostream& operator<< (ostream& is_p, const Slice& data_p);
	int getSize() const;
};

int Slice::getSize() const{
	return (bottomRightCell_m->x_m - topLeftCell_m->x_m + 1) * (bottomRightCell_m->y_m - topLeftCell_m->y_m + 1);
}

ostream& operator<<(ostream& os_p, const Slice& data_p){
	os_p << data_p.topLeftCell_m->y_m << " " << data_p.topLeftCell_m->x_m << " " << data_p.bottomRightCell_m->y_m << " " << data_p.bottomRightCell_m->x_m;
//	os_p << "[" << data_p.topLeftCell_m->x_m << "][" << data_p.topLeftCell_m->y_m << "] to [" << data_p.bottomRightCell_m->x_m << "][" << data_p.bottomRightCell_m->y_m << "]";
	return os_p;
}

Slice::Slice(Cell* top_p, Cell* bottom_p):
		topLeftCell_m(top_p),
		bottomRightCell_m(bottom_p)
{
}

class PizzaProblem{
public:
	int verticalSize_m;
	int horizontalSize_m;
	int minOfEachIngredientPerSlice_m;
	int maxOfIngredientPerSlice_m;
	char** pizza_m;
	bool** isAssigned_m;
	Slice* minSize(int,int);
	bool isUnreachable(int,int);
	void assign(Slice*);
	bool addUnreachableCellsToSlice(Slice*);
};

bool PizzaProblem::isUnreachable(int x,int y){
	bool res_l = (minSize(x,y) == 0);
//	if (res_l){
//		cout << x << " " << y << (res_l ? " NOT REACHABLE" : " REACHABLE") << "\n";
//	}
	return res_l;
}

Slice* PizzaProblem::minSize(int x, int y){
	int minSize_l = verticalSize_m*horizontalSize_m;
	Slice* bestSlice_l = 0;
	for (int i = 0; i < horizontalSize_m; i++){
		for (int j = 0; j < verticalSize_m; j++){
			//La taille de la part qu'on considere
			int sliceSize_l = (abs(i-x)+1)*(abs(j-y)+1);
			//Si part trop grande, KO
			if (sliceSize_l > maxOfIngredientPerSlice_m){
				continue;
			}
			//On remet les coins dans le bon ordre
			int minX_l = min(i,x);
			int maxX_l = max(i,x);
			int minY_l = min(j,y);
			int maxY_l = max(j,y);
			int tomatoCounter_l = 0;
			int mushroomCounter_l = 0;
//			cout << i << " " << x << " " << j << " " << y << " " << minX_l << " " << maxX_l << " " << minY_l << " " << maxY_l << "\n";
			bool isPossibleSlice_l = true;
			for (int k = minX_l; k <= maxX_l; k++){
				for (int l = minY_l; l <= maxY_l; l++){
					if (pizza_m[k][l] == 'T'){
						tomatoCounter_l++;
					}
					else{
						mushroomCounter_l++;
					}
					if (isAssigned_m[k][l]){
						isPossibleSlice_l = false;
						break;
					}
					if (tomatoCounter_l >= minOfEachIngredientPerSlice_m && mushroomCounter_l >= minOfEachIngredientPerSlice_m){
						if (sliceSize_l < minSize_l){
							minSize_l = sliceSize_l;
							bestSlice_l = new Slice(new Cell(minX_l,minY_l),new Cell(maxX_l,maxY_l));
//													cout << "NEW MIN = " << minSize_l << "\n";
							break;
						}
					}
				}
				if (!isPossibleSlice_l){
					break;
				}
			}
		}
	}
	return bestSlice_l;
}

PizzaProblem read(string fileName_p){
	ifstream myFile_l;
	myFile_l.open(fileName_p.c_str());
	PizzaProblem pb_l;
	myFile_l >> pb_l.verticalSize_m >> pb_l.horizontalSize_m >> pb_l.minOfEachIngredientPerSlice_m >> pb_l.maxOfIngredientPerSlice_m;
	
	pb_l.pizza_m = new char*[pb_l.horizontalSize_m];
	pb_l.isAssigned_m = new bool*[pb_l.horizontalSize_m];

	for (int i = 0; i < pb_l.horizontalSize_m; i++){
		pb_l.pizza_m[i] = new char[pb_l.verticalSize_m];
		pb_l.isAssigned_m[i] = new bool[pb_l.verticalSize_m];
	}

	for (int i = 0; i < pb_l.verticalSize_m; i++){
		for (int j = 0; j < pb_l.horizontalSize_m; j++){
			myFile_l >> pb_l.pizza_m[j][i];
			pb_l.isAssigned_m[j][i] = false;
		}
	}

//	for (int i = 0; i < pb_l.verticalSize_m; i++){
//		for (int j = 0; j < pb_l.horizontalSize_m; j++){
//			cout << pb_l.pizza_m[j][i];
//		}
//		cout << "	\n";
//	}

	myFile_l.close();
	return pb_l;
}

void PizzaProblem::assign(Slice* slice_p){
	for (int i = slice_p->topLeftCell_m->x_m; i <= slice_p->bottomRightCell_m->x_m; i++){
		for (int j = slice_p->topLeftCell_m->y_m; j <= slice_p->bottomRightCell_m->y_m; j++){
			isAssigned_m[i][j] = true;
		}
	}
}

bool PizzaProblem::addUnreachableCellsToSlice(Slice* slice_p){

	int sliceSize_l = (slice_p->bottomRightCell_m->x_m - slice_p->topLeftCell_m->x_m+1)*(slice_p->bottomRightCell_m->y_m-slice_p->topLeftCell_m->y_m+1);
	int enhancedSliceSizeVertical_l = sliceSize_l + slice_p->bottomRightCell_m->x_m - slice_p->topLeftCell_m->x_m + 1;
	int enhancedSliceSizeHorizontal_l = sliceSize_l + slice_p->bottomRightCell_m->y_m - slice_p->topLeftCell_m->y_m + 1;

	//	cout << sliceSize_l << " " << enhancedSliceSizeHorizontal_l << " " << enhancedSliceSizeVertical_l << "\n";

	//Ajout de la ligne en bas !
	int line_l = slice_p->bottomRightCell_m->y_m + 1;
	bool isPossibleAssignment_l = true;
	if (line_l < verticalSize_m && enhancedSliceSizeVertical_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){
			if (isAssigned_m[i][line_l]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){

				if (isUnreachable(i,line_l)){
					//				cout << slice_p->bottomRightCell_m->y_m << "\n";
					slice_p->bottomRightCell_m->y_m = line_l;
					//				cout << slice_p->bottomRightCell_m->y_m << "\n";
					for (int k = slice_p->topLeftCell_m->x_m; k < slice_p->bottomRightCell_m->x_m + 1; k++){
						isAssigned_m[k][line_l] = true;
					}
					//				cout << "line " << line_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
			}
		}
	}

	//Ajout de la ligne en haut
	line_l = slice_p->topLeftCell_m->y_m - 1;
	isPossibleAssignment_l = true;
	if (line_l >= 0 && enhancedSliceSizeVertical_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){
			if (isAssigned_m[i][line_l]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){
				if (isUnreachable(i,line_l)){
					slice_p->topLeftCell_m->y_m = line_l;
					for (int k = slice_p->topLeftCell_m->x_m; k < slice_p->bottomRightCell_m->x_m + 1; k++){
						isAssigned_m[k][line_l] = true;
					}
					//				cout << "line " << line_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
			}
		}
	}

	//Ajout de la ligne sur le cote gauche
	int column_l = slice_p->topLeftCell_m->x_m - 1;
	isPossibleAssignment_l = true;

	if (column_l >= 0 && enhancedSliceSizeHorizontal_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){
			if (isAssigned_m[column_l][i]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){
				if (isUnreachable(column_l,i)){
					slice_p->topLeftCell_m->x_m = column_l;
					for (int k = slice_p->topLeftCell_m->y_m; k < slice_p->bottomRightCell_m->y_m + 1; k++){
						isAssigned_m[column_l][k] = true;
					}
					//				cout << "column " << column_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
			}
		}
	}

	//Ajout de la ligne sur le cote droit
	column_l = slice_p->bottomRightCell_m->x_m + 1;
	isPossibleAssignment_l = true;
	if (column_l < horizontalSize_m && enhancedSliceSizeHorizontal_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){
			if (isAssigned_m[column_l][i]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){

				if (isUnreachable(column_l,i)){
					slice_p->bottomRightCell_m->x_m = column_l;
					for (int k = slice_p->topLeftCell_m->y_m; k < slice_p->bottomRightCell_m->y_m + 1; k++){
						isAssigned_m[column_l][k] = true;
					}
					//				cout << "column " << column_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
			}
		}
	}

	return false;
}

void optimize(PizzaProblem* pb_p){

	vector<Slice*> slices_l;
	int nbSlices_l = 0;

	for (int i = 0; i < pb_p->horizontalSize_m; i++){
		for (int j = 0; j < pb_p->verticalSize_m; j++){

			if (pb_p->isAssigned_m[i][j]){
//				cout << "[" << i << "][" << j << "] is assigned\n";
				continue;
			}
			Slice* bestSlice_l = pb_p->minSize(i,j);
			if (bestSlice_l){
//				cout << "MIN SLICE IS " << *bestSlice_l << "\n";
				pb_p->assign(bestSlice_l);
				while(pb_p->addUnreachableCellsToSlice(bestSlice_l));
				slices_l.push_back(bestSlice_l);
//				cout << *bestSlice_l << "\n";
			}
//			else{
//				cout << "no slice found for " << i << " " << j << "\n";
//			}
		}
	}

	cout << slices_l.size() << "\n";
	for (int i = 0; i < slices_l.size(); i++){
		cout << *(slices_l[i]) << "\n";
	}
//	cout << (pb_p->isUnreachable(2,2) ? "NOT REACHABLE" : "REACHABLE") << "\n";
//	if (pb_p->minSize(0,2)){
//		cout << *(pb_p->minSize(0,2)) << "\n";
//	}

//	int maxSize_l = 0;
//	for (int i = 0; i < pb_p->verticalSize_m; i++){
//		for (int j = 0; j < pb_p->horizontalSize_m; j++){
//			Slice* bestSlice_l = pb_p->minSize(j,i);
//			if (bestSlice_l){
//				cout << "Minimum slice containing [" << j << "][" << i << "] = " << *bestSlice_l << "\n";
//				int size_l = bestSlice_l->getSize();
//				if (size_l > maxSize_l){
//					maxSize_l = size_l;
//					maxSlice_l = bestSlice_l;
//				}
//			}
//			break;
//		}
//	}
//	if (maxSlice_l){
//		cout << "MAX SLICE IS " << *maxSlice_l << "\n";
////		for (int i = maxSlice_l->topLeftCell_m->x_m; i <= maxSlice_l->bottomRightCell_m->x_m; i++){
////			for (int j = maxSlice_l->topLeftCell_m->y_m;
////		}
//	}
}

int main(int argc, char** argv){
	PizzaProblem pb_l = read(string(argv[1]));
	optimize(&pb_l);
	for (int j = 0; j < pb_l.verticalSize_m; j++){
		for (int i = 0; i < pb_l.horizontalSize_m; i++ ){
			if (pb_l.isAssigned_m[i][j]){
				cout << " ";
			}
			else{
				cout << pb_l.pizza_m[i][j];
			}
		}
		cout << "\n";
	}
	return 0;
};
