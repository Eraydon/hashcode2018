#include<iostream>
#include<fstream>
#include<cstdlib>
#include "VehicleObjects.cpp"

class VehicleAssignmentGreedy: public GreedyAssignment<Vehicle,Ride>{
protected:
	int timeStep_m;
public:
	VehicleAssignmentGreedy(VehicleWorkAssignmentProblem*);
	virtual const Vehicle* chooseAssignee();
	virtual void update(const Ride*, const Vehicle*);
	virtual void init(){};
	bool isBetter(const Ride*, const Ride*, const Vehicle*);
};

bool VehicleAssignmentGreedy::isBetter(const Ride* firstRide_p, const Ride* secondRide_p, const Vehicle* vehicle_p){
	if (!firstRide_p) return false;
	if (!secondRide_p) return true;
	int nbTasks_l = this->workProblem_m->assignment_m[vehicle_p->getIndex()].size();
	const Ride* lastRide_l = (nbTasks_l > 0 ? this->workProblem_m->assignment_m[vehicle_p->getIndex()][nbTasks_l - 1] : 0);
	int distance1_l = max(Ride::getDistance(lastRide_l,firstRide_p),firstRide_p->getEarliestBegin()-timeStep_m);
	int distance2_l = max(Ride::getDistance(lastRide_l,secondRide_p),secondRide_p->getEarliestBegin()-timeStep_m);

	if (timeStep_m + distance1_l + firstRide_p->getLength() < 0.98*static_cast<const VehicleAssignmentProblem*>(this->workProblem_m->getProblem())->getNbSteps()){
		int minValue_l = static_cast<const VehicleWorkAssignmentProblem*>(this->workProblem_m)->minDistToNext_m[firstRide_p->getIndex()];
		distance1_l += minValue_l;
	}

	if (distance1_l == distance2_l){
		int constraint1_l = firstRide_p->getLatestFinish() - firstRide_p->getEarliestBegin();
		int constraint2_l = secondRide_p->getLatestFinish() - secondRide_p->getEarliestBegin();
		return constraint1_l < constraint2_l;
	}
	return distance1_l < distance2_l;
}

const Vehicle* VehicleAssignmentGreedy::chooseAssignee(){
	timeStep_m = 0;
	return GreedyAssignment<Vehicle,Ride>::chooseAssignee();
}

void VehicleAssignmentGreedy::update(const Ride* ride_p, const Vehicle* vehicle_p){
	int nbTasks_l = this->workProblem_m->assignment_m[vehicle_p->getIndex()].size();
	const Ride* lastRide_l = (nbTasks_l > 0 ? this->workProblem_m->assignment_m[vehicle_p->getIndex()][nbTasks_l - 1] : 0);
	int distance_l = Ride::getDistance(lastRide_l,ride_p);
	timeStep_m = max(ride_p->getEarliestBegin(), distance_l + timeStep_m) + ride_p->getLength();
}

VehicleAssignmentGreedy::VehicleAssignmentGreedy(VehicleWorkAssignmentProblem* pb_p):
		GreedyAssignment<Vehicle,Ride>(pb_p)
{
}

VehicleAssignmentProblem read(string fileName_p){
	vector<Ride> rides_l;
	vector<Vehicle> vehicles_l;

	ifstream myFile_l;
	myFile_l.open(fileName_p.c_str());
	int verticalSize_l, horizontalSize_l, nbVehicles_l, nbRides_l, bonus_l, nbSteps_l;
	myFile_l >> verticalSize_l >> horizontalSize_l >> nbVehicles_l >> nbRides_l >> bonus_l >> nbSteps_l;
	
	for (int ride_l = 0; ride_l < nbRides_l; ride_l++){
		Ride newRide_l(ride_l);
		myFile_l >> newRide_l;
		newRide_l.init();
		rides_l.insert(rides_l.end(),newRide_l);
	}

	for (int vehicle_l = 0; vehicle_l < nbVehicles_l; vehicle_l++){
		Vehicle newVehicle_l(vehicle_l);
		vehicles_l.insert(vehicles_l.end(),newVehicle_l);
	}

	myFile_l.close();

	return VehicleAssignmentProblem(verticalSize_l, horizontalSize_l, nbVehicles_l, nbRides_l, bonus_l, nbSteps_l, rides_l, vehicles_l);
}

int main(int argc, char** argv){
	VehicleAssignmentProblem pb_l = read(string(argv[1]));
	VehicleWorkAssignmentProblem workPb_l(&pb_l);
	VehicleAssignmentGreedy greedy_l(&workPb_l);
	cout << "OPTIMIZE\n";
	greedy_l.optimize();
	cout << "RESULT\n";
	workPb_l.printSolution();
	cout << "\nSCORE = " << workPb_l.eval() << "\n";
	VehicleSwap aswap_l(&workPb_l);
	aswap_l.optimize();
	workPb_l.printSolution();
	cout << "\nSCORE = " << workPb_l.eval() << "\n";
};
