#include<iostream>
#include<fstream>
#include<cstdlib>
#include<vector>

using namespace std;

class Cell{
public:
	Cell(int,int);
	int x_m;
	int y_m;
};

Cell::Cell(int x,int y):
		x_m(x),
		y_m(y)
{
}

class Slice{
public:
	Slice(Cell*,Cell*);
	Cell* topLeftCell_m;
	Cell* bottomRightCell_m;
	friend ostream& operator<< (ostream& is_p, const Slice& data_p);
	int getSize() const;
};

int Slice::getSize() const{
	return (bottomRightCell_m->x_m - topLeftCell_m->x_m + 1) * (bottomRightCell_m->y_m - topLeftCell_m->y_m + 1);
}

ostream& operator<<(ostream& os_p, const Slice& data_p){
	os_p << data_p.topLeftCell_m->y_m << " " << data_p.topLeftCell_m->x_m << " " << data_p.bottomRightCell_m->y_m << " " << data_p.bottomRightCell_m->x_m;
//	os_p << "[" << data_p.topLeftCell_m->x_m << "][" << data_p.topLeftCell_m->y_m << "] to [" << data_p.bottomRightCell_m->x_m << "][" << data_p.bottomRightCell_m->y_m << "]";
	return os_p;
}

Slice::Slice(Cell* top_p, Cell* bottom_p):
		topLeftCell_m(top_p),
		bottomRightCell_m(bottom_p)
{
}

class PizzaProblem{
public:
	int verticalSize_m;
	int horizontalSize_m;
	int minOfEachIngredientPerSlice_m;
	int maxOfIngredientPerSlice_m;
	char** pizza_m;
	bool** isAssigned_m;
	Slice* minSize(int,int);
	bool isUnreachable(int,int);
	void assign(Slice*);
	bool addUnreachableCellsToSlice(Slice*);
	bool isValid(Slice*);
	Slice* backtrack(vector<Slice*>&,int);
};

bool PizzaProblem::isUnreachable(int x,int y){
	bool res_l = (minSize(x,y) == 0);
	//	if (res_l){
	//		cout << x << " " << y << (res_l ? " NOT REACHABLE" : " REACHABLE") << "\n";
	//	}
	return res_l;
}

Slice* PizzaProblem::minSize(int x, int y){
	int minSize_l = verticalSize_m*horizontalSize_m;
	Slice* bestSlice_l = 0;

	int minLoopX_l = max(0,x-maxOfIngredientPerSlice_m+1);
	int maxLoopX_l = x;

	for (int i = minLoopX_l; i < maxLoopX_l+1; i++){

		int minLoopJ_l = min(horizontalSize_m-1,i+2*minOfEachIngredientPerSlice_m-1);
		int maxLoopJ_l = min(horizontalSize_m-1,i+maxOfIngredientPerSlice_m-1);

		for (int j = minLoopJ_l; j < maxLoopJ_l+1; j++){
//			cout << "TEST DE " << i << " a " << j << "\n";
			//La taille de la part qu'on considere
			int sliceSize_l = j-i;
			int tomatoCounter_l = 0;
			int mushroomCounter_l = 0;
			for (int k = i; k < j+1; k++){
				if (pizza_m[k][y] == 'T'){
					tomatoCounter_l++;
				}
				else{
					mushroomCounter_l++;
				}
				if (isAssigned_m[k][y]){
//					cout << "BREAK SUR " << k << " " << y << "\n";
					break;
				}
				if (tomatoCounter_l >= minOfEachIngredientPerSlice_m && mushroomCounter_l >= minOfEachIngredientPerSlice_m){
					if (sliceSize_l < minSize_l){
						minSize_l = sliceSize_l;
						bestSlice_l = new Slice(new Cell(i,y),new Cell(j,y));
						break;
					}
				}
			}
		}
	}
	if (bestSlice_l)
		cout << "BEST = " << *bestSlice_l << "\n";
	return bestSlice_l;
}

bool PizzaProblem::isValid(Slice* slice_p){
	int tomato_l = 0, mushroom_l = 0;
	for (int j = 0; j < slice_p->getSize(); j++){
		char c = pizza_m[slice_p->topLeftCell_m->x_m+j][slice_p->topLeftCell_m->y_m];
		if (c == 'T'){
			tomato_l++;
		}
		else{
			mushroom_l++;
		}
	}
	return (slice_p->getSize() <= maxOfIngredientPerSlice_m && tomato_l >= minOfEachIngredientPerSlice_m && mushroom_l >= minOfEachIngredientPerSlice_m);
}

PizzaProblem read(string fileName_p){
	ifstream myFile_l;
	myFile_l.open(fileName_p.c_str());
	PizzaProblem pb_l;
	myFile_l >> pb_l.verticalSize_m >> pb_l.horizontalSize_m >> pb_l.minOfEachIngredientPerSlice_m >> pb_l.maxOfIngredientPerSlice_m;
	
	pb_l.pizza_m = new char*[pb_l.horizontalSize_m];
	pb_l.isAssigned_m = new bool*[pb_l.horizontalSize_m];

	for (int i = 0; i < pb_l.horizontalSize_m; i++){
		pb_l.pizza_m[i] = new char[pb_l.verticalSize_m];
		pb_l.isAssigned_m[i] = new bool[pb_l.verticalSize_m];
	}

	for (int i = 0; i < pb_l.verticalSize_m; i++){
		for (int j = 0; j < pb_l.horizontalSize_m; j++){
			myFile_l >> pb_l.pizza_m[j][i];
			pb_l.isAssigned_m[j][i] = false;
		}
	}

//	for (int i = 0; i < pb_l.verticalSize_m; i++){
//		for (int j = 0; j < pb_l.horizontalSize_m; j++){
//			cout << pb_l.pizza_m[j][i];
//		}
//		cout << "	\n";
//	}

	myFile_l.close();
	return pb_l;
}

void PizzaProblem::assign(Slice* slice_p){
	for (int i = slice_p->topLeftCell_m->x_m; i <= slice_p->bottomRightCell_m->x_m; i++){
		for (int j = slice_p->topLeftCell_m->y_m; j <= slice_p->bottomRightCell_m->y_m; j++){
			isAssigned_m[i][j] = true;
		}
	}
}

bool PizzaProblem::addUnreachableCellsToSlice(Slice* slice_p){

	int sliceSize_l = (slice_p->bottomRightCell_m->x_m - slice_p->topLeftCell_m->x_m+1)*(slice_p->bottomRightCell_m->y_m-slice_p->topLeftCell_m->y_m+1);
	int enhancedSliceSizeVertical_l = sliceSize_l + slice_p->bottomRightCell_m->x_m - slice_p->topLeftCell_m->x_m + 1;
	int enhancedSliceSizeHorizontal_l = sliceSize_l + slice_p->bottomRightCell_m->y_m - slice_p->topLeftCell_m->y_m + 1;

	//	cout << sliceSize_l << " " << enhancedSliceSizeHorizontal_l << " " << enhancedSliceSizeVertical_l << "\n";

	//Ajout de la ligne en bas !
	int line_l = slice_p->bottomRightCell_m->y_m + 1;
	bool isPossibleAssignment_l = true;
	if (line_l < verticalSize_m && enhancedSliceSizeVertical_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){
			if (isAssigned_m[i][line_l]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){

				if (isUnreachable(i,line_l)){
					//				cout << slice_p->bottomRightCell_m->y_m << "\n";
					slice_p->bottomRightCell_m->y_m = line_l;
					//				cout << slice_p->bottomRightCell_m->y_m << "\n";
					for (int k = slice_p->topLeftCell_m->x_m; k < slice_p->bottomRightCell_m->x_m + 1; k++){
						isAssigned_m[k][line_l] = true;
					}
					//				cout << "line " << line_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
			}
		}
	}

	//Ajout de la ligne en haut
	line_l = slice_p->topLeftCell_m->y_m - 1;
	isPossibleAssignment_l = true;
	if (line_l >= 0 && enhancedSliceSizeVertical_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){
			if (isAssigned_m[i][line_l]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->x_m; i < slice_p->bottomRightCell_m->x_m + 1; i++){
				if (isUnreachable(i,line_l)){
					slice_p->topLeftCell_m->y_m = line_l;
					for (int k = slice_p->topLeftCell_m->x_m; k < slice_p->bottomRightCell_m->x_m + 1; k++){
						isAssigned_m[k][line_l] = true;
					}
					//				cout << "line " << line_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
			}
		}
	}

	//Ajout de la ligne sur le cote gauche
	int column_l = slice_p->topLeftCell_m->x_m - 1;
	isPossibleAssignment_l = true;

	if (column_l >= 0 && enhancedSliceSizeHorizontal_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){
			if (isAssigned_m[column_l][i]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){
				if (isUnreachable(column_l,i)){
					slice_p->topLeftCell_m->x_m = column_l;
					for (int k = slice_p->topLeftCell_m->y_m; k < slice_p->bottomRightCell_m->y_m + 1; k++){
						isAssigned_m[column_l][k] = true;
					}
					//				cout << "column " << column_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
			}
		}
	}

	//Ajout de la ligne sur le cote droit
	column_l = slice_p->bottomRightCell_m->x_m + 1;
	isPossibleAssignment_l = true;
	if (column_l < horizontalSize_m && enhancedSliceSizeHorizontal_l <= maxOfIngredientPerSlice_m){
		for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){
			if (isAssigned_m[column_l][i]){
				isPossibleAssignment_l = false;
				break;
			}
		}
		if (isPossibleAssignment_l){
			for (int i = slice_p->topLeftCell_m->y_m; i < slice_p->bottomRightCell_m->y_m + 1; i++){
				if (isUnreachable(column_l,i)){
					slice_p->bottomRightCell_m->x_m = column_l;
					for (int k = slice_p->topLeftCell_m->y_m; k < slice_p->bottomRightCell_m->y_m + 1; k++){
						isAssigned_m[column_l][k] = true;
					}
					//				cout << "column " << column_l << " added\n";
					//				cout << *slice_p << "\n";
					return true;
				}
				else{
					cout << "break reachable " << column_l << " " << i << "\n";
				}
			}
		}
	}

	return false;
}

Slice* PizzaProblem::backtrack(vector<Slice*>& slices_p,int index_p){
	int x = slices_p[index_p]->bottomRightCell_m->x_m+1;
	int y = slices_p[index_p]->bottomRightCell_m->y_m;

	cout << "BACKTRACK!\n";

	for (int k = 0; k < maxOfIngredientPerSlice_m - 2*minOfEachIngredientPerSlice_m; k++){
		isAssigned_m[slices_p[index_p]->bottomRightCell_m->x_m][slices_p[index_p]->topLeftCell_m->y_m] = false;
		cout << "DESAFF " << slices_p[index_p]->bottomRightCell_m->x_m << " " << slices_p[index_p]->topLeftCell_m->y_m << "\n";
		slices_p[index_p]->bottomRightCell_m->x_m = slices_p[index_p]->bottomRightCell_m->x_m-1;
//		cout << "ON CHERCHE " << x << " " << y << "\n";
		Slice* currentSlice_p = minSize(x,y);
		if (currentSlice_p){
			assign(currentSlice_p);
			while (addUnreachableCellsToSlice(currentSlice_p));
			cout << *currentSlice_p << " is valid\n";
			if (index_p+1 < slices_p.size()){
				slices_p[index_p+1] = currentSlice_p;
				cout << "SLICE " << index_p+1 << " is " << *currentSlice_p << "\n";
			}
			if (isValid(slices_p[index_p])){
				assign(slices_p[index_p]);
				cout << "previous slice " << *(slices_p[index_p]) << " is also valid!\n";
				cout << "SLICE " << index_p << " is " << *currentSlice_p << "\n";
				return currentSlice_p;
			}
			else{
				if (index_p > 1){
					for (int i = slices_p[index_p]->topLeftCell_m->x_m; i <= slices_p[index_p]->bottomRightCell_m->x_m; i++){
						isAssigned_m[i][y] = false;
					}
					if(backtrack(slices_p, index_p-1)){
						return currentSlice_p;
					}
					else{
						return 0;
					}
				}
			}
		}
	}

	for (int k = 0; k < maxOfIngredientPerSlice_m - 2*minOfEachIngredientPerSlice_m; k++){
		slices_p[index_p]->bottomRightCell_m->x_m = slices_p[index_p]->bottomRightCell_m->x_m+1;
	}

	assign(slices_p[index_p]);

	return 0;
}

void optimize(PizzaProblem* pb_p){

	vector<Slice*> slices_l;
	int nbSlices_l = 0;
	int nbIterations_l = 0;
	int value_l = 0;
	cout << "Iteration " << ++nbIterations_l << "\n";

	Slice* prevSlice_l = 0;

	for (int j = 0; j < pb_p->verticalSize_m; j++){

		int i = 0;
		int increment_l = 0;
		prevSlice_l = 0;

		while (i < pb_p->horizontalSize_m){

			if (prevSlice_l){
				i = min(pb_p->horizontalSize_m-1, pb_p->maxOfIngredientPerSlice_m + prevSlice_l->topLeftCell_m->x_m+increment_l);
			}
			else{
				i = increment_l;
			}

			cout << "ON VEUT COUVRIR " << i << " " << j << "\n";

			Slice* bestSlice_l = 0;
			if (pb_p->minSize(i,j)){
				int end_l = min(pb_p->horizontalSize_m-1,i+pb_p->maxOfIngredientPerSlice_m-1);
				bestSlice_l = new Slice(new Cell(i,j),new Cell(end_l,j));
			}
			else{
				if (prevSlice_l){
					vector<Slice*> oldSlices_l = slices_l;
					bestSlice_l = pb_p->backtrack(slices_l,slices_l.size()-1);
					if (!bestSlice_l){
						slices_l = oldSlices_l;
					}
				}
			}

			if (bestSlice_l){
				increment_l = 0;
				cout << *bestSlice_l << "\n";
				pb_p->assign(bestSlice_l);
//				if (prevSlice_l){
//					while(pb_p->addUnreachableCellsToSlice(prevSlice_l));
//				}
				slices_l.push_back(bestSlice_l);
				if (prevSlice_l){
					value_l += prevSlice_l->getSize();
				}
				prevSlice_l = bestSlice_l;
			}
			else{
				if (i == pb_p->horizontalSize_m-1){
					if (prevSlice_l){
						value_l+=prevSlice_l->getSize();
					}
					break;
				}
				increment_l++;
			}
		}
	}

	cout << slices_l.size() << "\n";

	for (int i = 0; i < slices_l.size(); i++){
		cout << *(slices_l[i]);
		if (pb_p->isValid(slices_l[i])){
			cout << " is valid!";
		}
		else{
			cout << " is not valid!";
		}
		cout << "\n";
	}

	cout << "VALUE = " << value_l << "\n";
}

int main(int argc, char** argv){
	PizzaProblem pb_l = read(string(argv[1]));
	optimize(&pb_l);
	for (int j = 0; j < pb_l.verticalSize_m; j++){
		for (int i = 0; i < pb_l.horizontalSize_m; i++ ){
			if (pb_l.isAssigned_m[i][j]){
				cout << " ";
			}
			else{
				cout << pb_l.pizza_m[i][j];
			}
		}
		cout << "\n";
	}
	return 0;
};
