DATE=`date +%HH%MM%Ss`

#Creation des repertoires
if [ ! -d res ]; then
	mkdir res;
fi

#Repertoire date pour eviter d'ecraser des resultats
mkdir res/$DATE

#On copie le binaire pour pouvoir rejouer le run
cp HashCode19.cpp res/$DATE

#On lance tous les runs
for i in *.txt; do
    ./hashcode.exe $i > res/$DATE/$i.out;
done

#On met a jour le lien symbolique du lastRun
cd res
if [ -L lastRun ]; then
	unlink lastRun;
fi
    
ln -s $DATE lastRun

cd ..
