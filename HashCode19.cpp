#include<iostream>
#include<fstream>
#include<cstdlib>
#include<vector>
#include<algorithm>
#include<map>

using namespace std;

//Definition des objets
//Premiere classe : un objet avec des attributs simples
class Photo{
public:
	Photo();
	int index_m;
	//H = Horizontal, V = Vertical
	char format_m;
	//Tags
	int nbTags_m;
	//Tags
	vector<string> tags_m;
	//Lecture d'un objet
	friend istream& operator>> (istream&,Photo&);
	//Ecriture d'un objet
	friend ostream& operator<< (ostream&,const Photo&);
	//La photo est elle assignée à un slide ?
	bool isAssigned_m;
};

Photo::Photo():
		isAssigned_m(false)
{

}
//Lecture d'un objet avec un vecteur
istream& operator>>(istream& is_p, Photo& myObj_p){
	is_p >> myObj_p.format_m >> myObj_p.nbTags_m;

	for (int i = 0; i < myObj_p.nbTags_m; i++){
		string tag_l;
		is_p >> tag_l;
		myObj_p.tags_m.push_back(tag_l);
	}

	return is_p;
}

//Ecriture d'un objet avec un vecteur
ostream& operator<<(ostream& os_p, const Photo& myObj_p){
	os_p << myObj_p.format_m << myObj_p.nbTags_m;

	for (int i = 0; i < myObj_p.nbTags_m; i++){
		os_p << " " << myObj_p.tags_m[i];
	}

	return os_p;
}

class Slide{
public:
	Slide();
	Slide(Photo*);
	Slide(Photo*,Photo*);
	Photo* firstPhoto_m;
	Photo* secondPhoto_m;
	vector<string> tags_m;
	bool isAssigned_m;

	//Common tags
	int evalCombinationWith(Slide*);
	int evalInSlideAndNotInOther(Slide*);
	int evalCommon(Slide*);

	//Lecture d'un objet
	friend istream& operator>> (istream&,Slide&);
	//Ecriture d'un objet
	friend ostream& operator<< (ostream&,const Slide&);
	void setTags();
};

int Slide::evalInSlideAndNotInOther(Slide* slide_p){
	int value_l = 0;
	for (unsigned int i = 0; i < tags_m.size(); i++){
		for (unsigned int j = 0; j < slide_p->tags_m.size(); j++){
			if (slide_p->tags_m[j].compare(tags_m[i]) == 0){
				break;
			}
			if (j == slide_p->tags_m.size()-1){
				value_l++;
			}
		}
	}
	return value_l;
}

int Slide::evalCommon(Slide* slide_p){
	int value_l = 0;
	for (unsigned int i = 0; i < tags_m.size(); i++){
		for (unsigned int j = 0; j < slide_p->tags_m.size(); j++){
//			cout << tags_m[i] << " " << slide_p->tags_m[j] << "\n";
			if (tags_m[i].compare(slide_p->tags_m[j]) == 0){
				value_l++;
				break;
			}
		}
	}
	return value_l;
}


int Slide::evalCombinationWith(Slide* slide_p){

	std::sort(tags_m.begin(),tags_m.end());
	std::sort(slide_p->tags_m.begin(),slide_p->tags_m.end());

	int nb1_l = evalInSlideAndNotInOther(slide_p);
	int nb2_l = slide_p->evalInSlideAndNotInOther(this);
	int nb3_l = evalCommon(slide_p);

//	int res_l = std::min(nb1_l,(min(nb2_l,nb3_l)));
//	cout << "EVAL COMBINATION " << *this << " and " << *slide_p << " = " << res_l << "(COMMON = " << nb3_l << ", DIFF = " << nb1_l << ", DIFF = " << nb2_l << ")\n";

	return std::min(nb1_l,(min(nb2_l,nb3_l)));
}

Slide::Slide():
		firstPhoto_m(0),
		secondPhoto_m(0),
		tags_m(0),
		isAssigned_m(false)
{

}

Slide::Slide(Photo* photo_p):
		firstPhoto_m(photo_p),
		secondPhoto_m(0),
		tags_m(photo_p->tags_m)
{
}

Slide::Slide(Photo* photo1_p,Photo* photo2_p):
		firstPhoto_m(photo1_p),
		secondPhoto_m(photo2_p),
		tags_m(0)
{
	std::set_union(photo1_p->tags_m.begin(), photo1_p->tags_m.end(), photo2_p->tags_m.begin(), photo2_p->tags_m.end(), std::back_inserter(tags_m));
//	cout<<"Test Concat 2 slides " << photo1_p << " et "<<photo2_p<< " vs ";
//	for (int i=0; i< tags_m.size(); i++){
//		cout << tags_m[i];
//	}
//	cout<<endl;
}

void Slide::setTags(){
	if(!secondPhoto_m){
		this->tags_m = this->firstPhoto_m->tags_m;
	}
	else{
		std::set_union(this->firstPhoto_m->tags_m.begin(), this->firstPhoto_m->tags_m.end(), this->secondPhoto_m->tags_m.begin(), this->secondPhoto_m->tags_m.end(), std::back_inserter(tags_m));
	}
}

//Lecture d'un objet avec un vecteur
istream& operator>>(istream& is_p, Slide& myObj_p){
	return is_p;
}

//Ecriture d'un objet avec un vecteur
ostream& operator<<(ostream& os_p, const Slide& myObj_p){
	if (myObj_p.firstPhoto_m){
		os_p << myObj_p.firstPhoto_m->index_m;
	}

	if (myObj_p.secondPhoto_m){
		os_p << " " << myObj_p.secondPhoto_m->index_m;
	}

	os_p << "\n";

	return os_p;
}

class SlideShow{
public:
	SlideShow();
	int nbSlides_m;
	vector<Slide*> vSlides_m;
	//Lecture d'un objet
	friend istream& operator>> (istream&,Photo&);
	//Ecriture d'un objet
	friend ostream& operator<< (ostream&,const Photo&);
};

SlideShow::SlideShow():
		nbSlides_m(0)
{

}

//Lecture d'un objet avec un vecteur
istream& operator>>(istream& is_p, SlideShow& myObj_p){
	return is_p;
}

//Ecriture d'un objet avec un vecteur
ostream& operator<<(ostream& os_p, const SlideShow& myObj_p){
	os_p << myObj_p.nbSlides_m << "\n";

	for (int i = 0; i < myObj_p.nbSlides_m; i++){
		os_p << *(myObj_p.vSlides_m[i]);
	}

	return os_p;
}

//Definition de la classe probleme
class MyProblem{
public:
	MyProblem();
	//Le nombre de photos
	int nbPhotos_m;
	vector<Photo> vPhoto_m;
	//Le vecteur d'objets qui represente la solution, type a revoir
	SlideShow slideShow_m;
	vector<Slide*> vHorizontalSlides_m;
	vector<Slide*> vVerticalSlides_m;
	vector<Slide*> vSortedSlides_m;
	//On optimise !
	void optimize();
	void optimize2();
	bool isNeededToContinue(Slide*,int);
	//On evalue
	int eval();
	int singleValue(Photo&);
	//On verifie la validite de la solution
	bool isValid();
	//Pour lire le fichier d'entree
	friend istream& operator>> (istream&,MyProblem&);
	//Pour ecrire le fichier de sortie
	friend ostream& operator<< (ostream&,const MyProblem&);

	void createVerticalSlide();
	void createHorizontalSlide();
	void createDummyVerticalSlide();
	void sortSlidesBySize();
};

MyProblem::MyProblem(){
}

ostream& operator<<(ostream& os_p, const MyProblem& myPb_p){
	//On ecrit un element dans os_p
	os_p << myPb_p.nbPhotos_m << "\n";

	//On ecrit en boucle les objets simples
	int maxIndex_l = myPb_p.nbPhotos_m;
	for (int index_l = 0; index_l < maxIndex_l; index_l++){
		os_p << myPb_p.vPhoto_m[index_l] << "\n";
	}

	return os_p;
}

istream& operator>>(istream& is_p, MyProblem& myPb_p){
	//Lecture des attributs
	is_p >> myPb_p.nbPhotos_m;

	//Boucle sur les objets
	for (int index_l = 0; index_l < myPb_p.nbPhotos_m; index_l++ ){
		//On lit l'objet
		Photo myPhoto_l;
		is_p >> myPhoto_l;
		//On l'ajoute au vecteur
		myPhoto_l.index_m = index_l;
		myPb_p.vPhoto_m.push_back(myPhoto_l);
	}

	return is_p;
}

int MyProblem::singleValue(Photo& myObj_p){
	return 0;
}

int MyProblem::eval(){
	int value_l = 0;
//	for (int index_l = 0; index_l < nbPhoto_m; index_l++){
//		value_l+= singleValue(vPhoto_m[index_l]);
//	}
//	cout << "eval = " << value_l << "\n";
	return value_l;
}

bool MyProblem::isValid(){
	return true;
}

void readInput(string fileName_p,MyProblem& pb_p){
	//On ouvre le flux vers le fichier
	ifstream myFile_l;
	myFile_l.open(fileName_p.c_str());

	//On lit le fichier pour creer le probleme, tout est dans la methode >> de MyProblem
	myFile_l >> pb_p;

	//Fermeture du fichier
	myFile_l.close();
}

void MyProblem::createDummyVerticalSlide(){
	Photo* firstPhoto_l = 0;
	for (unsigned int index_l = 0; index_l < vPhoto_m.size(); index_l++){
		if (vPhoto_m[index_l].format_m == 'V' && vPhoto_m[index_l].isAssigned_m == false)
		{
			if (!firstPhoto_l){
				firstPhoto_l = &vPhoto_m[index_l];
				vPhoto_m[index_l].isAssigned_m = true;
			} else {
				vVerticalSlides_m.push_back(new Slide(firstPhoto_l,&vPhoto_m[index_l]));

				firstPhoto_l = 0;
			}
		}

}
}


void MyProblem::createVerticalSlide(){
	Photo* firstPhoto_l = 0;
	for (unsigned int index_l = 0; index_l < vPhoto_m.size(); index_l++){
		if (!firstPhoto_l){
			if (vPhoto_m[index_l].format_m == 'V' && vPhoto_m[index_l].isAssigned_m == false)
			{
				firstPhoto_l = &vPhoto_m[index_l];
				vPhoto_m[index_l].isAssigned_m = true;
			}
		}
			else{
				Photo* bestSecondPhoto_l = 0;
				int bestNbCommonTags_l = 99999;
				for (unsigned int index2_l = 0; index2_l < vPhoto_m.size(); index2_l++){
					Photo* candidateSecondPhoto_l = 0;
					if (vPhoto_m[index2_l].format_m == 'V' && vPhoto_m[index2_l].isAssigned_m == false)
					{
						candidateSecondPhoto_l = &vPhoto_m[index2_l];
						int nbCommonTags_l = 0;
						for( int indexTag1_l = 0; indexTag1_l < firstPhoto_l->nbTags_m; indexTag1_l++){

							for( int indexTag2_l = 0; indexTag2_l < candidateSecondPhoto_l->nbTags_m; indexTag2_l++){
								if(candidateSecondPhoto_l->tags_m[indexTag2_l] == firstPhoto_l->tags_m[indexTag1_l]){
									nbCommonTags_l++;
								}
							}
						}

						if (nbCommonTags_l < bestNbCommonTags_l)
						{
							bestSecondPhoto_l = &vPhoto_m[index2_l];
							bestNbCommonTags_l = nbCommonTags_l;
						}
					}
				}
				vVerticalSlides_m.push_back(new Slide(firstPhoto_l,bestSecondPhoto_l));
				bestSecondPhoto_l->isAssigned_m = true;
				firstPhoto_l = 0;
			}

	}
}

void MyProblem::createHorizontalSlide(){
	for (unsigned int index_l = 0; index_l < vPhoto_m.size(); index_l++){
			if (vPhoto_m[index_l].format_m == 'H'){
				vHorizontalSlides_m.push_back(new Slide(&vPhoto_m[index_l]));
			}
		}
}

void MyProblem::sortSlidesBySize(){
	map<int, vector<Slide*> > innerMap_m;
	for (int i = 0; i<vHorizontalSlides_m.size(); i++){
		innerMap_m[(vHorizontalSlides_m[i])->tags_m.size()].push_back(vHorizontalSlides_m[i]);
	}
	for (int i = 0; i<vVerticalSlides_m.size(); i++){
		innerMap_m[(vVerticalSlides_m[i])->tags_m.size()].push_back(vVerticalSlides_m[i]);
	}
	map<int, vector<Slide*> >::iterator iter_l;
	for (iter_l = innerMap_m.begin(); iter_l != innerMap_m.end(); iter_l++){
		vector<Slide*>::iterator slide_l;
		for (slide_l = iter_l->second.begin(); slide_l!=iter_l->second.end(); slide_l++){
			vSortedSlides_m.push_back(*slide_l);
		}
	}
	//cout << vSortedSlides_m.size() << " vs " << vVerticalSlides_m.size()<<" "<<vHorizontalSlides_m.size()<<endl;
}

void MyProblem::optimize(){

	createHorizontalSlide();
	createDummyVerticalSlide();
	sortSlidesBySize();

	int nbSlides_l = vHorizontalSlides_m.size() + vVerticalSlides_m.size();

	for (int i = 0; i < nbSlides_l; i++){
		//On prend le premier horizontal arbitrairement
		if (i == 0){
			slideShow_m.vSlides_m.push_back(vHorizontalSlides_m[0]);
			vHorizontalSlides_m[0]->isAssigned_m = true;
			slideShow_m.nbSlides_m++;
		}
		else{
			int maxScore_l = -1;

			Slide* bestSlide_l = 0;
			for (unsigned int j = 0; j < vHorizontalSlides_m.size(); j++){
				if (vHorizontalSlides_m[j]->isAssigned_m){
					continue;
				}
				int eval_l = slideShow_m.vSlides_m[i-1]->evalCombinationWith(vHorizontalSlides_m[j]);



				if (eval_l > maxScore_l){
					maxScore_l = eval_l;
					bestSlide_l = vHorizontalSlides_m[j];
					if(!isNeededToContinue(bestSlide_l,maxScore_l)){
						cout << "BEST IS " << *bestSlide_l << "\n";
						slideShow_m.vSlides_m.push_back(bestSlide_l);
						slideShow_m.nbSlides_m++;
						bestSlide_l->isAssigned_m = true;
						break;
					}
				}
			}

			if(!isNeededToContinue(bestSlide_l,maxScore_l)){
				cout << "BEST IS " << *bestSlide_l << "\n";
				slideShow_m.vSlides_m.push_back(bestSlide_l);
				slideShow_m.nbSlides_m++;
				bestSlide_l->isAssigned_m = true;
				continue;
			}

			for (unsigned int j = 0; j < vVerticalSlides_m.size(); j++){
				if (vVerticalSlides_m[j]->isAssigned_m){
					continue;
				}
				int eval_l = slideShow_m.vSlides_m[i-1]->evalCombinationWith(vVerticalSlides_m[j]);
				if (eval_l > maxScore_l){
					maxScore_l = eval_l;
					bestSlide_l = vVerticalSlides_m[j];
					if(!isNeededToContinue(bestSlide_l,maxScore_l)){
						cout << "BEST IS " << *bestSlide_l << "\n";
						slideShow_m.vSlides_m.push_back(bestSlide_l);
						slideShow_m.nbSlides_m++;
						bestSlide_l->isAssigned_m = true;
						break;
					}
				}
			}
			cout << "BEST IS " << *bestSlide_l << "\n";
			slideShow_m.vSlides_m.push_back(bestSlide_l);
			slideShow_m.nbSlides_m++;
			bestSlide_l->isAssigned_m = true;
		}
	}

//	for (unsigned int index_l = 0; index_l < vHorizontalSlides_m.size(); index_l++){
//		slideShow_m.vSlides_m.push_back(vHorizontalSlides_m[index_l]);
//		indexSlideShow_l++;
//		slideShow_m.nbSlides_m++;
//	}
//
//	for (unsigned int index_l = 0; index_l < vVerticalSlides_m.size(); index_l++){
//		slideShow_m.vSlides_m.push_back(vVerticalSlides_m[index_l]);
//		indexSlideShow_l++;
//		slideShow_m.nbSlides_m++;
//	}
//	cout << slideShow_m;
}


void MyProblem::optimize2(){

	createHorizontalSlide();
	createDummyVerticalSlide();
	sortSlidesBySize();

	int nbSlides_l = vSortedSlides_m.size();

	for (int i = 0; i < nbSlides_l; i++){
		//On prend le premier horizontal arbitrairement
		if (i == 0){
			slideShow_m.vSlides_m.push_back(vSortedSlides_m[0]);
			vSortedSlides_m[0]->isAssigned_m = true;
			slideShow_m.nbSlides_m++;
		}
		else{
			int maxScore_l = -1;

			Slide* bestSlide_l = 0;
			for (unsigned int j = 0; j < vSortedSlides_m.size(); j++){
				if (vSortedSlides_m[j]->isAssigned_m){
					continue;
				}
				int eval_l = slideShow_m.vSlides_m[i-1]->evalCombinationWith(vSortedSlides_m[j]);

				if (eval_l > maxScore_l){
					maxScore_l = eval_l;
					bestSlide_l = vSortedSlides_m[j];
					if(!isNeededToContinue(bestSlide_l,maxScore_l)){
						cout << "BEST IS " << *bestSlide_l << "\n";
						slideShow_m.vSlides_m.push_back(bestSlide_l);
						slideShow_m.nbSlides_m++;
						bestSlide_l->isAssigned_m = true;
						break;
					}
				}
			}

			cout << "BEST IS " << *bestSlide_l << "\n";
			slideShow_m.vSlides_m.push_back(bestSlide_l);
			slideShow_m.nbSlides_m++;
			bestSlide_l->isAssigned_m = true;
		}
	}
}

bool MyProblem::isNeededToContinue(Slide* bestSlide_l,int maxScore_l ){

	if(!bestSlide_l->secondPhoto_m){//Cas horizontal

		if(maxScore_l == (bestSlide_l->firstPhoto_m->nbTags_m/2)  ){
			//cout << "1 max score found " << *bestSlide_l << "\n";
			return 0;
		}
	}
	else{
		if(maxScore_l == (bestSlide_l->tags_m.size()/2 ) ){
			//cout << "2 max score found " << *bestSlide_l << "\n";
			return 0;
		}
	}

	return 1;

}

int main(int argc, char** argv){
	//Creation du probleme
	MyProblem pb_l;

	//Lecture des fichiers
	readInput(string(argv[1]),pb_l);

	//Optimisation
	pb_l.optimize2();

	//Evaluation
	pb_l.eval();

	//Ecriture output
	cout << pb_l.slideShow_m;

//	pb_l.slideShow_m.vSlides_m[0]->evalCombinationWith(pb_l.slideShow_m.vSlides_m[1]);

	return 0;
};
